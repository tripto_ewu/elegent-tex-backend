<?php

namespace App\Http\Controllers;

use App\Type;
use Illuminate\Http\Request;

class TypesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $types = Type::all();
        return \Response::json($types);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $type = new Type();
        $type->type_name = $request->productType;
        $type->save();
        $type = Type::find($type->id);
        return \Response::json($type);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Type  $types
     * @return \Illuminate\Http\Response
     */
    public function show(Type $types)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Type  $types
     * @return \Illuminate\Http\Response
     */
    public function edit(Type $types)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Type  $types
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Type $types)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Type  $types
     * @return \Illuminate\Http\Response
     */
    public function destroy(Type $types)
    {
        //
    }
}
