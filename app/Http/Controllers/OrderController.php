<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Events\NewOrder;
use App\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::with('products')->with('customer')->with('user')->orderBy('id', 'DESC')->paginate(5);
        $orders->getCollection()->transform(function ($order) {
            $order_data = [
                'id' => $order->id,
                'user' => $order->user->first_name,
                'delevery_mathod' => $order->delevery_mathod,
                'delevery_date' => $order->delevery_date,
                'total_amount' => $order->total_amount,
                'status' => $order->status,
                'note' => $order->note,
                'created_at' => $order->created_at,
                'customer' => $order->customer,
                'products' => $this->formateProductData($order->products)
            ];
            return $order_data;
        });
        return $orders;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $order = new Order();
        $order->user_id = $request->userId;
        $order->delevery_mathod = $request->deleveryMethod;
        $order->delevery_date = $request->deleveryDate;
        $order->total_amount = $request->totalAmount;
        $order->note = $request->notes;
        $order->save();
        $order = Order::find($order->id);
        $order->customer()->create([
            'name' => $request->customerName,
            'phone_no' => $request->phone,
            'address' => $request->address,
            'division' => $request->division,
            'district' => $request->district,
            'upazila' => $request->upazila
        ]);
        $order->products()->attach($request->products);
        $order_product = $order->products;
        $products = $this->formateProductData($order_product);

        $completeOrder = [
            'id' => $order->id,
            'user' => $order->user->first_name,
            'delevery_date' => $order->delevery_date,
            'delevery_mathod' => $order->delevery_mathod,
            'note' => $order->note,
            'status' => $order->status,
            'total_amount' => $order->total_amount,
            'created_at' => $order->created_at,
            'customer' => $order->customer,
            'products' => $products
        ];
        event(new NewOrder($completeOrder));
        return \Response::json($completeOrder);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function formateProductData($data)
    {
        $products = [];
        foreach ($data as $product) {
            $product_arr = [
                'id' => $product->id,
                'name' => $product->product_name,
                'price' => $product->pivot->price,
                'productColor' => $product->pivot->productColor,
                'productType' => $product->pivot->productType,
                'quantity' => $product->pivot->quantity,

            ];
            array_push($products, $product_arr);
        }
        return $products;
    }
}
