<?php

namespace App\Http\Controllers;

use App\Services\UserServices;
use App\User;
use Illuminate\Http\Request;


class UserController extends Controller
{
    public function __construct(UserServices $userServices)
    {
        $this->userServices = $userServices;
    }


    public function register (Request $request) {
        //dd($request);
        return $this->userServices->createNewUser($request);
    }
    public function uploadImage (Request $request, $id) {

        $user = User::find($id);
        if ($user->image_link){
            $usersImage = public_path()."/storage/images/".$user->image_link; // get previous image from folder
            if (\File::exists($usersImage)) { // unlink or remove previous image from folde
                unlink($usersImage);
            }
        }

        $data = $request->all();
        foreach ($data as $key => $file) {
            $fileNameWithExt = $file->getClientOriginalName();
            $fileName = pathinfo($fileNameWithExt, PATHINFO_FILENAME);
            $extention = $file->getClientOriginalExtension();
            $fileNameToStore = $fileName.'_'.time().'.'.$extention;
            $file->storeAs('/public/images', $fileNameToStore);
            return \Response::json($fileNameToStore);
        };

    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->first_name = $request->firstName;
        $user->last_name= $request->lastName;
        $user->email= $request->email;
        $user->phone_no= $request->phone;
        $user->image_link= $request->image;
        $user->save();
        return \Response::json($user);
    }

}
