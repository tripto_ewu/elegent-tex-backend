<?php

namespace App\Http\Controllers;

use App\District;
use App\Division;
use App\Upazila;
use Illuminate\Http\Request;

class RegionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $districts = District::all();
        return \Response::json($districts);

    }
    public function getAllDivisions()
    {
        $divisions = Division::all();
        return \Response::json($divisions);

    }
    public function getSelectedDistricts(Request $request)
    {
        $division_id = $request->division_id;

        $districts = District::where('division_id', $division_id)->orderBy('name','asc')->get();
        return \Response::json($districts);

    }
    public function getSelectedUpazilas(Request $request)
    {
        $district_id = $request->district_id;

        $upazilas = Upazila::where('district_id', $district_id)->orderBy('name','asc')->get();
        return \Response::json($upazilas);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
