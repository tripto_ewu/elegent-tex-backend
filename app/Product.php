<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function types()
    {
        return $this->belongsToMany(Type::class);
    }
    public function orders()
    {
        return $this->belongsToMany(Order::class);
    }
}
