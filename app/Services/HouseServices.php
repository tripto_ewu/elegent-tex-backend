<?php


namespace App\Services;


use App\House;
use App\User;
use App\User_detail;


class HouseServices
{
    public function createHouse($data)
    {

        $user = User::findOrFail($data['user_id']);
        $userDetailsCount = User_detail::where('user_id','=', $data['user_id'])->count();
        if ($userDetailsCount == 0){
            $userDetailsService = new UserDetailsServices();
            $userDetailsService->createUserDetails($data);
        }
        $house = new House();
        $house->house_name = $data['houseName'];
        $house->floor_count = $data['floorCount'];
        $house->save();
        $user->houses()->attach($house->id);
        return $house;
    }

    public function getHousesById($id) {
        $user = User::findOrFail($id);
        $houses = $user->houses()->get();
        return $houses;
    }
}
