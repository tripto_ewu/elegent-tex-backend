<?php


namespace App\Services;


use App\User;
use Illuminate\Support\Facades\Hash;

class UserServices
{
    public function createNewUser($request)
    {
        //dd($request);
        try {
            $user = new User();
            $user->first_name = $request->firstName;
            $user->last_name = $request->lastName;
            $user->email = $request->email;
            $user->phone_no = $request->phone;
            $user->password = Hash::make($request->password);
            $user->save();
            $token = auth()->login($user);
            return response()->json([
                'access_token' => $token,
                'token_type' => 'bearer',
                'expires_in' => auth()->factory()->getTTL() * 60
            ]);
        } catch (\Illuminate\Database\QueryException $e) {
            $errorCode = $e->errorInfo[1];
            if ($errorCode == 1062) {
                throw new \Exception($e);
            } else {
                throw $e;
            }
        }
    }
    public function getUserHousesById($id)
    {
        $user = User::with('details', 'houses')->get();
        return \Response::json($user);
    }
}
