<?php


namespace App\Services;


use App\User;
use App\User_detail;


class UserDetailsServices
{
    public function createUserDetails($data)
    {
        $user = User::find($data['user_id']);
        $user_details = new User_detail();
        $user_details->phn_no = $data['phone'];
        $user_details->nid_no = $data['nid'];
        $user_details->division_id = $data->division;
        $user_details->district_id = $data->district;
        $user_details->upazila_id = $data->upazila;
        $user_details->house_no = $data->houseNo;
        $user_details->street = $data->street;
        $user_details->ward_or_union = $data->wardOrUnion;
        $user->details()->save($user_details);
        return $user_details;
    }
}
