<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});


Route::post('register', 'UserController@register');
Route::post('updateUserProfile/{id}', 'UserController@update');
Route::post('addProductColor', 'ColorsController@store');
Route::get('getAllColors', 'ColorsController@index');
Route::post('updateProductColor/{id}', 'ColorsController@update');
Route::get('deleteProductColor/{id}', 'ColorsController@destroy');
Route::post('addProduct/', 'ProductController@store');
Route::post('addProductType/', 'TypesController@store');
Route::get('getAllTypes/', 'TypesController@index');
Route::post('updateProduct/{id}', 'ProductController@update');
Route::get('getAllProducts/', 'ProductController@index');
Route::get('deleteProduct/{id}', 'ProductController@destroy');
Route::post('uploadProfileImage/{id}', 'UserController@uploadImage');
Route::get('/getAllDivisions', 'RegionController@getAllDivisions');
Route::post('/getSelectedDistricts', 'RegionController@getSelectedDistricts');
Route::post('/getSelectedUpazilas', 'RegionController@getSelectedUpazilas');
Route::post('/createNewOrder', 'OrderController@store');
Route::get('/getAllOrders', 'OrderController@index');
Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
});
